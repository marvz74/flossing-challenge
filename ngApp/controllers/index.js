'use strict';

/* App */

var FlossingChallenge = angular.module('floss', ['ngRoute','routers','ngSanitize']);

(function( ng, app ) {

	function loginCtrl( $scope) {


	};

	loginCtrl.prototype = {

	};

	function homeCtrl( $scope, $timeout) {
		$scope.fbName = "Guest";
							
		//facebook login
		$scope.fbLogin = function(){
			FB.login();
		}

		//twitter login
		$scope.ttLogin = function(){
			FB.login();
		}
		this.init();
	};

	homeCtrl.prototype = {

		init: function(){
			window.fbAsyncInit = function() {
		        FB.init({appId:'169677393071841',xfbml:true,version:'v2.0'});

				FB.getLoginStatus(function(response) {
		          if (response.status === 'connected') {
		            console.log("connected")
		          	$('#socialNetworkLogin').modal('hide');
					FB.api('/me', function(response) {
						$scope.fbName = response.name;
						$scope.$digest($scope.fbName);
				    	console.log('Successful login for: ' + response);
				    	//document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
				    });
		          }
		          else {
		            $('#socialNetworkLogin').modal({show: true,backdrop:'static',keyboard:false});
		            FB.Event.subscribe('auth.statusChange', function(){
						FB.api('/me', function(response) {
							$('#socialNetworkLogin').modal('hide');
							$scope.fbName = response.name;
							$scope.$apply($scope.fbName);
					    	console.log('Successful login for: ' + response);
					    });
		            });
		          }
		        });
		  	};
		},
		update: function(param){

		},
		delete: function(param){

		}

	};

	//----//

	function challengeCtrl( $scope, $q,$http,$timeout) {
		console.log("challenge")

		this.q = $q;
		this.http = $http;
		this.scope = $scope;		
		this.timeout = $timeout;

		//facebook login
		$scope.fbLogin = function(){
			FB.login();
		}

		//twitter login
		$scope.ttLogin = function(){
			FB.login();
		}

		//this.scope.attachment = ng.bind( this, this._attachment);
		
		//get challenge fields
		
		this.fields();
		this.init();
	};

	challengeCtrl.prototype = {
		init: function(){
			var self = this;
			window.fbAsyncInit = function() {
				FB.init({appId:'169677393071841',xfbml:true,version:'v2.0'});
				FB.getLoginStatus(function(response) {
					if (response.status === 'connected') {

						console.log("challenge logged in")

						$('#socialNetworkLogin').modal('hide');
						FB.api('/me', function(response) {
							self.details = {email: response.email};
							self.update('api/v1/challenge/create', self.details);
						});
					}
					else
					{
						$('#socialNetworkLogin').modal({show: true,backdrop:'static',keyboard: false});
					}
		        });
			}
		},
		fields: function(){
			var self = this;
			var q = self.q.defer();
			var promise = self.timeout(function(){return q.promise;},100);			
			var def = this.find('api/v1/challenge/show','');
			def.then(function(pp){
				self.scope.fields = pp;
				q.resolve(def);
			});
		},
		find: function(uri, param){

			var deferred = this.q.defer();
			var p = param;
			this.http({method: 'get', url: uri,data: param}).
			success(function(data, status, headers, config) {
				deferred.resolve(data);
			}).
			error(function(data, status, headers, config){
				deferred.reject(data);
			});
			return deferred.promise;
		},
		update: function(uri, param){
			var deferred = this.q.defer();
			var p = param;
			this.http({method: 'POST', url: uri,data: param}).
			success(function(data, status, headers, config) {
				deferred.resolve(data);
			}).
			error(function(data, status, headers, config){
				deferred.reject(data);
			});
			return deferred.promise;
		},
		delete: function(param){

		},

	};

	function fieldsCtrl( $scope, $q,$http, $route, $routeParams) {

	this.scope = $scope;

	}

	fieldsCtrl.prototype = {

	}

	app.controller( "login", loginCtrl );
	app.controller( "home", homeCtrl );
	app.controller( "challenge", challengeCtrl );
	app.controller( "fields", fieldsCtrl );

})( angular, FlossingChallenge );


/*filter*/
FlossingChallenge.filter('sprintf', function() {
    function parse(str) {
        var args = [].slice.call(arguments, 1),
            i = 0;
    
        return str.replace(/%s/g, function() {
            return args[i++];
        });
    }
 	//Limit into 5 arguments
    return function(str) {
        return parse(str, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
    };
});



/*directive*/
FlossingChallenge.directive('formBuilder', function(){
	return {
	    restrict: "A",
	    scope: {
	    	handleSave: '=save',
	    	dsave: "=linkSave",
	    	dload: "=linkLoad"
	    },
	    link: function(scope, element, attrs) {

	    	console.log(scope.dsave)
	    	console.log(scope.dload)

			if(scope.handleSave)
			{
				element.formbuilder({
		          'save_url': scope.dsave,
		          //'load_url': load_url,
		          'useJson' : true,
		        });	
			}else if(scope.handleSave == false){
		        element.formbuilder({
					//'save_url': save_url,
					'load_url': load_url,
					'useJson' : true,
		        });
			}
			else{
		        element.formbuilder({
		        	'save_url': scope.dsave,
			        'load_url': scope.dload,
			        'useJson' : true,
		        });
			}
	    	element.find('ul').sortable({ opacity: 0.6, cursor: 'move'});
	    }
	}
});


FlossingChallenge.directive('buildForms', function(){
	return {

	    restrict: "A",
	    scope: {value:"=field"},
	    template: '<div ng-repeat="field in fields"><span ng-bind-html="field"></span></div>',
	    link: function(scope, element, attrs) { },
	    controller: function($scope, $timeout, $filter, $sce){

	    	$timeout(function(){
	    		$scope.fields = [];
	    		angular.forEach($scope.value, function(value, key){
			    	var html = loadField(value);				
			    	$scope.fields.push($sce.trustAsHtml(html));
		    	});
	    	}, 500);

	    	function loadField(field){

	    		if(!angular.isArray(field) && angular.isObject(field))
	    		{
	    			console.log(field['cssClass']);
					switch(field['cssClass']){
						case 'input_text':
							return loadInputText(field);
							break;
						case 'textarea':
							return loadTextarea(field);
							break;
						case 'checkbox':
							return loadCheckboxGroup(field);
							break;
						case 'radio':
							return loadRadioGroup(field);
							break;
						case 'select':
							return loadSelectBox(field);
							break;
    				}
	    		}
	    		return false;
	    	}//EOF loadField

	    	function loadInputText(field){

	    		field['required'] = field['required'] == 'checked' ? ' required' : false;
				var html = '';
				html += "";
				html += '<div class="clearfix">';
				//html += $filter('sprintf')('<li class="%s%s" id="fld-%s">' + "\n", field['cssClass'], field['required'], field['values']);
				html += $filter('sprintf')('<label class="col-sm-3 control-label" for="%s">%s</label>' + "\n", field['values'], field['values']);
				html += $filter('sprintf')('<input type="text" id="%s" name="%s" value="%s" />' + "\n", field['values'], field['values'], field['values']);
				//html += '</li>' + "\n";
				html += '</div>';
				return html;
	    	}//EOF loadInputText

	    	function loadTextarea(field){
			
				field['required'] = field['required'] == 'checked' ? ' required' : false;
				var html= '';
				html += '<div class="clearfix">';
				//html += $filter('sprintf')('<li class="%s%s" id="fld-%s">', field['cssClass'], field['required'], field['values']);
				html += $filter('sprintf')('<label class=" " or="%s">%s</label>', field['values'], field['values']);
				html += $filter('sprintf')('<textarea class="form-control" id="%s" name="%s" rows="5" cols="50">%s</textarea>',field['values'],field['values'],field['values']);
				//html += '</li>';
				html += '</div>';
				return html;
			}//EOF loadTextarea

			function loadCheckboxGroup(field){

				field['required'] = field['required'] == 'checked' ? ' required' : false;
				
				var html= '';

				//html += $filter('sprintf')('<li class="%s%s" id="fld-%s">', field['cssClass'], field['required'], field['title']);
				
				if(field['title'])
				{
					html += $filter('sprintf')('<label for="inputEmail3" class="col-sm-3 control-label" >%s</label>', field['title']);
				}

				field['values'] = field['values'];				
				if(angular.isObject(field['values']))
				{
					html += $filter('sprintf')('<span class="multi-row clearfix">');

					angular.forEach(field['values'], function(item, key)
					{
						// set the default checked value
						var checked = item['baseline'] == 'true' ? true : false;
						// if checked, set html
						checked = checked ? ' checked="checked"' : '';
						var checkbox = '<label class="checkbox-inline"><input type="checkbox" id="%s" name="%s-%s" %s />%s</label>';
						html += $filter('sprintf')(checkbox, field['title'], field['title'], item['value'], checked, item['value']);
					});
					html += '</span>';
				}
				//html += '</li>';
				return html;
			}//EOF loadCheckboxGroup

			function loadRadioGroup(field){

				field['required'] = field['required'] == 'checked' ? ' required' : false;

				var html= '';

				if(field['title'])
				{
					html += $filter('sprintf')('<label for="inputEmail3" class="col-sm-3 control-label" >%s</label>', field['title']);
				}

				field['values'] = field['values'];	

				if(angular.isObject(field['values']))
				{
					html += $filter('sprintf')('<span class="multi-row clearfix">');

					angular.forEach(field['values'], function(item, key)
					{
						// set the default checked value
						var checked = item['baseline'] == 'true' ? true : false;
						// if checked, set html
						checked = checked ? ' checked="checked"' : '';
						var radio = '<label class="radio-inline"><input type="radio" id="%s" name="%s" %s />%s</label>';
						html += $filter('sprintf')(radio, field['title'], field['title'], checked, item['value']);
					});
				}

				return html;
			}//EOF loadRadioGroup

			function loadSelectBox(field){
				field['required'] = field['required'] == 'checked' ? ' required' : false;

				var html= '';

				if(field['title'])
				{
					html += $filter('sprintf')('<label for="inputEmail3" class="col-sm-3 control-label" >%s</label>', field['title']);
				}

				field['values'] = field['values'];	

				if(angular.isObject(field['values']))
				{
					var multiple = field['multiple'] == "true" || field['multiple'] == "checked" ? ' multiple="multiple"' : '';
					html += $filter('sprintf')('<select name="%s" id="%s"%s>', field['title'], field['title'], multiple);
					if(field['required']){ html += '<option value="">Selection Required</label>'; }
					
					angular.forEach(field['values'], function(item, key)
					{

						var checked = item['baseline'] == 'true' ? true : false;
						// if checked, set html
						checked = checked ? ' checked="checked"' : '';

						var option = '<option value="%s"%s>%s</option>';
						html += $filter('sprintf')(option, item['value'], checked, item['value']);
					});
					html += '</select>';

				}


				return html;
			}//EOF loadSelectBox
	    	



	    }
	}
});


FlossingChallenge.directive('ngBindHtmlUnsafe', ['$sce', function($sce){
    return {
        scope: {
            ngBindHtmlUnsafe: '=',
        },
        template: "<div ng-bind-html='trustedHtml'></div>",
        link: function($scope, iElm, iAttrs, controller) {
            $scope.updateView = function() {
                $scope.trustedHtml = $sce.trustAsHtml($scope.ngBindHtmlUnsafe);
            }

            $scope.$watch('ngBindHtmlUnsafe', function(newVal, oldVal) {
                $scope.updateView(newVal);
            });
        }
    };
}]);