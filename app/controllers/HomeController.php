<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	protected $layout = 'layout.base';

	public function index()
	{
		$this->layout->title = 'Home';		
		$this->layout->content = View::make('home.index');
	}

	public function what()
	{
		$this->layout->title = 'What';		
		$this->layout->content = View::make('home.what');
	}

	public function why()
	{
		$this->layout->title = 'Why';		
		$this->layout->content = View::make('home.why');
	}

	public function when()
	{
		$this->layout->title = 'When';		
		$this->layout->content = View::make('home.when');
	}

	public function how()
	{
		$this->layout->title = 'How';		
		$this->layout->content = View::make('home.how');
	}

	public function whatif()
	{
		$this->layout->title = 'What If';		
		$this->layout->content = View::make('home.whatif');
	}

	public function store(){
		$user = new User;
		$user->email = Input::get('email');
		$user->save();
		return User::where('user_id','=',$user->id)->get();
	}

	public function edit($id){
		$user = User::find($id);
		return $user;
	}

}
