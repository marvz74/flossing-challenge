<?php

class FieldsController extends \BaseController {

	protected $_form_array;
	protected $layout = 'layout.fields';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	*/
	public function index()
	{
		$this->layout->title = 'Fields';		
		$fields = Challengefields::all();
		$this->layout->content = View::make('fields.index')->with('fields', $fields);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$form_data = isset($_POST['frmb']) ? $_POST : false;
		$form = $this->htmlRender($form_data);
		$formFields = array('form_id'=>$form['form_id'],'form_structure'=>json_encode($form['form_structure']));

		$field = new Challengefields;
		$field->title = Input::get('title');
		$field->fields = json_encode($form['form_structure']);
		$field->save();
		return $formFields;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$fields = Challengefields::find($id);
		$formFields = array('form_structure'=>$fields->fields);		
		return 	$this->htmlRender((array)$formFields);		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->title = 'Fields';		
		$fields = Challengefields::find($id);
		$this->layout->content = View::make('fields.edit')->with('title', $fields->title)->with('fid', $id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$form_data = isset($_POST['frmb']) ? $_POST : false;
		$form = $this->htmlRender($form_data);
		$formFields = array('form_id'=>$form['form_id'],'form_structure'=>json_encode($form['form_structure']));

		$field = Challengefields::find($id);
		if(Input::get('title'))
		{
			$field->title = Input::get('title');	
		}		
		$field->fields = json_encode($form['form_structure']);
		$field->save();
		return $field;
	}

	/**
	 * setDefaul.
	 *
	*/
	public function setDefault($id)
	{
		$this->layout->title = 'Fields';		
		
		$fields = Challengefields::all();

		foreach($fields as $key=>$field)
		{
			if($field->challenge_fields_id	== $id)
			{
				$default = Challengefields::find($id);
				$default->default = 1;
				$default->save();
			}
			else
			{
				$default = Challengefields::find($field->challenge_fields_id);
				$default->default = 0;
				$default->save();
			}
		}

		return Redirect::to('fields');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function create()
	{
		$this->layout->title = 'Fields';
		$this->layout->content = View::make('fields.create');
	}

	public function forms()
	{
		$form = Array( 'form_structure' => '[{"cssClass":"input_text","required":"undefined","values":"First Name"},{"cssClass":"input_text","required":"undefined","values":"Last Name"},{"cssClass":"textarea","required":"undefined","values":"Bio"},{"cssClass":"checkbox","required":"undefined","title":"What\'s on your pizza?","values":{"2":{"value":"Extra Cheese","baseline":"undefined"},"3":{"value":"Pepperoni","baseline":"undefined"},"4":{"value":"Beef","baseline":"undefined"}}}]');

		return 	$this->htmlRender($form);
	}

	function htmlRender($form){

		$form = is_array($form) ? $form : array();

		if(array_key_exists('form_structure', $form))
		{
			$form['form_structure'] = json_decode($form['form_structure'], true);
			$this->_form_array = $form;
		}
		else if(array_key_exists('frmb', $form))
		{
			$_form = array();
			$_form['form_id'] = ($form['form_id'] == "undefined" ? false : $form['form_id']);
			$_form['form_structure'] = $form['frmb']; // since the form is from POST, set it as the raw array
			$this->_form_array = $_form;
		}

		return $this->_form_array;
	}

	public function generate_html($form_action = false){
		$html = '';
		$form_action = $form_action ? $form_action : $_SERVER['PHP_SELF'];
		if(is_array($this->_form_array['form_structure'])){
			$html .= '<form class="frm-bldr" method="post" action="'.$form_action.'">' . "\n";
			$html .= '<ol class="frmb">'."\n";
			foreach($this->_form_array['form_structure'] as $field){
				$html .= $this->loadField((array)$field);
			}
			$html .= '<li class="btn-submit"><input type="submit" name="submit" value="Submit" /></li>' . "\n";
			$html .=  '</ol>' . "\n";
			$html .=  '</form>' . "\n";
		}
		return $html;
	}

	protected function loadField($field){

		if(is_array($field) && isset($field['cssClass'])){

			switch($field['cssClass']){
				case 'input_text':
					return $this->loadInputText($field);
					break;
				case 'textarea':
					return $this->loadTextarea($field);
					break;
				case 'checkbox':
					return $this->loadCheckboxGroup($field);
					break;
				case 'radio':
					return $this->loadRadioGroup($field);
					break;
				case 'select':
					return $this->loadSelectBox($field);
					break;
			}
		}
		return false;
	}

	/**
	 * Returns html for an input type="text"
	 * 
	 * @param array $field Field values from database
	 * @access protected
	 * @return string
	 */
	protected function loadInputText($field){

		$field['required'] = $field['required'] == 'checked' ? ' required' : false;

		$html = '';
		$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['values']));
		$html .= sprintf('<label for="%s">%s</label>' . "\n", $this->elemId($field['values']), $field['values']);
		$html .= sprintf('<input type="text" id="%s" name="%s" value="%s" />' . "\n",
								$this->elemId($field['values']),
								$this->elemId($field['values']),
								$this->getPostValue($this->elemId($field['values'])));
		$html .= '</li>' . "\n";
		return $html;

	}

	/**
	 * Returns html for a <textarea>
	 *
	 * @param array $field Field values from database
	 * @access protected
	 * @return string
	 */
	protected function loadTextarea($field){

		$field['required'] = $field['required'] == 'checked' ? ' required' : false;
		$html = '';
		$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['values']));
		$html .= sprintf('<label for="%s">%s</label><br>' . "\n", $this->elemId($field['values']), $field['values']);
		$html .= sprintf('<textarea id="%s" name="%s" rows="5" cols="50">%s</textarea>' . "\n",
								$this->elemId($field['values']),
								$this->elemId($field['values']),
								$this->getPostValue($this->elemId($field['values'])));
		$html .= '</li>' . "\n";
		return $html;
	}


	/**
	 * Returns html for an <input type="checkbox"
	 *
	 * @param array $field Field values from database
	 * @access protected
	 * @return string
	 */
	protected function loadCheckboxGroup($field){

		$field['required'] = $field['required'] == 'checked' ? ' required' : false;

		$html = '';
		$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['title']));

		if(isset($field['title']) && !empty($field['title'])){
			$html .= sprintf('<span class="false_label">%s</span>' . "\n", $field['title']);
		}
		$field['values'] = (array)$field['values'];
		if(isset($field['values']) && is_array($field['values'])){
			$html .= sprintf('<span class="multi-row clearfix">') . "\n";
			foreach($field['values'] as $item){

				$item = (array)$item;

				// set the default checked value
				$checked = $item['default'] == 'true' ? true : false;

				// load post value
				$val = $this->getPostValue($this->elemId($item['value']));
				$checked = !empty($val);

				// if checked, set html
				$checked = $checked ? ' checked="checked"' : '';

				$checkbox 	= '<span class="row clearfix"><input type="checkbox" id="%s-%s" name="%s-%s" value="%s"%s /><label for="%s-%s">%s</label></span>' . "\n";
				$html .= sprintf($checkbox, $this->elemId($field['title']), $this->elemId($item['value']), $this->elemId($field['title']), $this->elemId($item['value']), $item['value'], $checked, $this->elemId($field['title']), $this->elemId($item['value']), $item['value']);
			}
			$html .= sprintf('</span>') . "\n";
		}
		$html .= '</li>' . "\n";
		return $html;

	}


	/**
	 * Returns html for an <input type="radio"
	 * @param array $field Field values from database
	 * @access protected
	 * @return string
	 */
	protected function loadRadioGroup($field){

		$field['required'] = $field['required'] == 'checked' ? ' required' : false;
		$html = '';
		$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['title']));
		if(isset($field['title']) && !empty($field['title'])){
			$html .= sprintf('<span class="false_label">%s</span>' . "\n", $field['title']);
		}
		$field['values'] = (array)$field['values'];
		if(isset($field['values']) && is_array($field['values'])){
			$html .= sprintf('<span class="multi-row">') . "\n";
			foreach($field['values'] as $item){

				$item = (array)$item;

				// set the default checked value
				$checked = $item['default'] == 'true' ? true : false;

				// load post value
				$val = $this->getPostValue($this->elemId($field['title']));
				$checked = !empty($val);

				// if checked, set html
				$checked = $checked ? ' checked="checked"' : '';

				$radio 		= '<span class="row clearfix"><input type="radio" id="%s-%s" name="%1$s" value="%s"%s /><label for="%1$s-%2$s">%3$s</label></span>' . "\n";
				$html .= sprintf($radio,
										$this->elemId($field['title']),
										$this->elemId($item['value']),
										$item['value'],
										$checked);
			}
			$html .= sprintf('</span>') . "\n";
		}

		$html .= '</li>' . "\n";
		return $html;
	}


	/**
	 * Returns html for a <select>
	 * 
	 * @param array $field Field values from database
	 * @access protected
	 * @return string
	 */
	protected function loadSelectBox($field){
		$field['required'] = $field['required'] == 'checked' ? ' required' : false;
		$html = '';
		$html .= sprintf('<li class="%s%s" id="fld-%s">' . "\n", $this->elemId($field['cssClass']), $field['required'], $this->elemId($field['title']));
		if(isset($field['title']) && !empty($field['title'])){
			$html .= sprintf('<label for="%s">%s</label>' . "\n", $this->elemId($field['title']), $field['title']);
		}
		$field['values'] = (array)$field['values'];
		if(isset($field['values']) && is_array($field['values'])){
			$multiple = $field['multiple'] == "true" || $field['multiple'] == "checked" ? ' multiple="multiple"' : '';
			$html .= sprintf('<select name="%s" id="%s"%s>' . "\n", $this->elemId($field['title']), $this->elemId($field['title']), $multiple);
			if($field['required']){ $html .= '<option value="">Selection Required</label>'; }

			foreach($field['values'] as $item){

				$item = (array)$item;

				// set the default checked value
				$checked = $item['default'] == 'true' ? true : false;

				// load post value
				$val = $this->getPostValue($this->elemId($field['title']));
				$checked = !empty($val);

				// if checked, set html
				$checked = $checked ? ' checked="checked"' : '';

				$option 	= '<option value="%s"%s>%s</option>' . "\n";
				$html .= sprintf($option, $item['value'], $checked, $item['value']);
			}
			$html .= '</select>' . "\n";
			$html .= '</li>' . "\n";
		}
		return $html;
	}
}