<?php

class ChallengesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	protected $layout = 'layout.base';
	
	public function index()
	{
		
		$this->layout->title = 'Challenge';		
		$this->layout->content = View::make('challenge.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return 2;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function store(){

		$user = new User;
		$user->email = Input::get('email');
		$user->save();
		return User::where('user_id','=',$user->id)->get();

	}

	public function edit($id){
		$user = User::find($id);
		return $user;
	}
}