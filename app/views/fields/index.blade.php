@section('content')



<!--div form-builder></div-->
<p><a href="fields/create">Create</a></p>
<div class="bs-example">


    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Default</th>
          <th width="65%">Tittle</th>
          <th>Date Created</th>
        </tr>
      </thead>
      <tbody>

      @foreach($fields as $key=>$field)
      <!--p>{{{$field->title}}}</p-->
        <tr>
          <td><a href="fields/edit/{{{$field->challenge_fields_id}}}">{{{$key+1}}}</a></td>
          <td align="center"><a href="{{{Request::root()}}}/fields/default/{{{$field->challenge_fields_id}}}">{{{$field->default}}}</a></td>
          <td>{{{$field->title}}}</td>
          <td>{{{$field->created_at}}}</td>
        </tr>
      @endforeach

      </tbody>
    </table>
</div>



@stop