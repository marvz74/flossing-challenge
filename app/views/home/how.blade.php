@section('content') 
<h2>How to floss?</h2>
<div class="jumbotron no-padding">      
<div class="responsive-video"><iframe width="560" height="315" src="http://www.youtube.com/embed/3oem-M2tQU4" frameborder="0" allowfullscreen=""></iframe></div>
</div>

<ul>

<li>Take a good length of dental floss (about 40 cm)</li>

<li>Wrap it around your fingers so there is just a small space of floss exposed.</li>

<li>Pass the floss gently in between the teeth- don’t push or saw too hard, you may hear a little click. </li>

<li>Gently wipe the floss down the one side of the tooth, move over the triangle of gum to the opposite tooth and do the same thing</li>

<li>Remove the floss, wrap on or pull it through to a fresh bit and repeat.</li>

<li>Work your way around your mouth.</li>

<li>Then give the floss a good wipe with a tissue or get a new piece of floss to do all the teeth in the other jaw.</li>

</ul>

@stop