@section('content') 

<div class="jumbotron">      
      <h3 class="no-margin"><a href="challenge">Start Challenge</a></h3>
      <br>
      
      <h3 style="color: #666;">Current number of flossing challengers: 320</h3>
      <h3><a data-toggle="modal" role="button" href="#topDonts">Top don'ts</a>,<a data-toggle="modal" role="button" href="#topDos">Top do's</a></h3>
      </div>
<div class="modal fade" id="topDonts" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4>Top Don'ts</h4>
            <ul>
            <li>
            Don’t just floss your front teeth 
            because they are easier to do- you 
            need to floss in-between all of your 
            teeth. It is important to do all of the 
            teeth, not just the front ones and the 
            more tricky (i.e. the very back teeth) 
            the floss is to get in, the more plaque 
            that is likely to have collected there 
            and the more beneficial it will be. 
            Persevere, you will get there and if the 
            angle or way you are doing it doesn't 
            seem to be working, just change it a 
            bit, until it does.
            </li>
            <li>
            Don’t saw into your gums you will 
            only hurt them- gently wipe down the 
            side of the teeth either side. If you are 
            too hard when you floss and saw into 
            your gums, you will cause soreness 
            and more bleeding.
            </li>
            </ul>
      </div>
    </div>
  </div>
</div>
<!-- Modal topDos-->
<div class="modal fade" id="topDos" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4>Top Do's</h4>
            <ul>
            <li>
            Do start in one position and end in 
            one position- i.e. your flossing must be 
            systematic.
            </li>
            <li>
            Do floss all your teeth.
            </li>
            <li>
            Do move along the floss to use a 
            fresh bit for in-between each contact.
            </li>
            <li>
            Do floss once every evening so you 
            are going to sleep without food and 
            plaque in-between your teeth. Saliva, 
            (your natural protection) dries up 
            when you go to sleep, so any sugar, 
            plaque and food stuck in-between 
            your teeth is much more damaging at 
            this time. Flossing in the morning isn’t 
            necessary (unless you want to.
            </li>
            </ul>
      </div>
    </div>
  </div>
</div>



      <div class="row marketing">
        <div class="col-lg-6">
          <h4>Subheading</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

          <h4>Subheading</h4>
          <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>

          <h4>Subheading</h4>
          <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>

        <div class="col-lg-6">
          <h4>Subheading</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

          <h4>Subheading</h4>
          <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>

          <h4>Subheading</h4>
          <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>
      </div>
@stop