
<!DOCTYPE html>
<html lang="en" ng-app="floss">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Flossing Challenge</title>
    <!-- Font-awesome CSS -->

    
    <link rel="stylesheet" href="{{{ URL::to('../styles/font-awesome.min.css'); }}}">

    <!-- Bootstrap core CSS -->
    <link href="{{{ URL::to('../styles/bootstrap.min.css'); }}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{{ URL::to('../styles/jumbotron-narrow.css'); }}}" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--formbuilder CSS-->
    <link href="{{{ URL::to('../styles/jquery.formbuilder.css'); }}}" media="screen" rel="stylesheet" />
</head>

<body>
      <div class="container">

      <div class="header clearfix">
        <div class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <img height="50" src="{{{Request::root()}}}/../img/Logo1.png" class="pull-left"/>
              <a class="navbar-brand" href="{{{Request::root()}}}">Flossing Challenge</a>

            </div>
            <div class="navbar-collapse collapse" style="height: 1px;">
              <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="what">What</a></li>
                <li><a href="why">Why</a></li>
                <li><a href="when">When</a></li>
                <li><a href="how">How</a></li>
                <li><a href="whatif">What If</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div><!--/.container-fluid -->
        </div>
        <!--div class="pull-right"><i class="icon-user"></i> <strong>{{fbName}}</strong></div-->
      </div>

<h2>Challenge Fields</h2>


@yield('content')

<!--form role="form">
  <div class="checkbox">
    <label>
      <input type="checkbox"> Publish
    </label>
  </div>
  <div class="form-group">
    <label for="">Label</label>
    <input type="text" class="form-control" id="" placeholder="Label here...">
  </div>
  <div class="form-group">
    <label for="">Type</label>
    <select class="form-control">
      <option selected="selected">Text</option>
      <option>Radio</option>
      <option>Checkbox</option>
      <option>Dropdown</option>
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Save</button>
</form-->








<br>
    <div class="footer">
      <p>&copy; Flossing Challenge 2014</p>
    </div>
  </div> <!-- /container -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="{{{ URL::to('../ngApp/lib/jquery-1.9.1.min.js'); }}}"></script>
    <script src="{{{ URL::to('../ngApp/lib/bootstrap.min.js'); }}}"></script>    
    <script src="{{{ URL::to('../ngApp/lib/angular.min.js'); }}}"></script>    
    <script src="{{{ URL::to('../ngApp/lib/angular-route.js'); }}}"></script>   

    <script>
      angular.module('routers',[])
      .config(["$routeProvider", function ($routeProvider) {
/*
        $routeProvider
          .when("/create/:id", {})
          .otherwise({ redirectTo: '/' });*/

      }]);
    </script>

    <script>
    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>

    <script src="{{{ URL::to('../ngApp/lib/jquery.formbuilder.js'); }}}"></script>

    <script src="{{{ URL::to('../ngApp/controllers/index.js'); }}}"></script>
    


  </body>
</html>