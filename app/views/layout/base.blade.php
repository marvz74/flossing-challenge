
<!DOCTYPE html>
<html lang="en" ng-app="floss">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Flossing Challenge</title>
    <!-- Font-awesome CSS -->
    <link rel="stylesheet" href="../styles/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="../styles/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../styles/jumbotron-narrow.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body ng-controller="home" >

       
<div class="bg-danger clearfix deset9"><strong >NOTES!</strong><br>
<ul class="clearfix">
<li>Migration databse not set-up.</li>
<li>Page Videos not set-up.</li>
<li>Social login email storing remove.</li>
<li>Challenge fields data still not able to save.</li>
<li>Challenge page routes not stable.</li>
<li>Fossing fact not created through backend</li>
<li>Challenge page user interaction need to automatically save.</li>
</ul>
</div>

    <div class="container">
      <div class="header clearfix">
        <div class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <img height="50" src="../img/Logo1.png" class="pull-left"/>
              <a class="navbar-brand" href="{{{Request::root()}}}">Flossing Challenge</a>

            </div>
            <div class="navbar-collapse collapse" style="height: 1px;">
              <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="what">What</a></li>
                <li><a href="why">Why</a></li>
                <li><a href="when">When</a></li>
                <li><a href="how">How</a></li>
                <li><a href="whatif">What If</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div><!--/.container-fluid -->
        </div>
        <div class="pull-right"><i class="icon-user"></i> <strong>{{fbName}}</strong></div>
      </div>
      
      
      @yield('content')
        


      <div class="footer">
        <p>&copy; Flossing Challenge 2014</p>
      </div>

    </div> <!-- /container -->

<!-- Social Network -->
<div class="modal fade inset9" id="socialNetworkLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2 class="no-margin">LOGIN USING</h2>
        <div class="text-center">

        <ul class="list-unstyled inset8">
          <li><a href="#/facebook" ng-click="fbLogin()"><img src="../img/facebook-btn.png" /></a></li>
          <li><a href="#/twitter" ng-click="ttLogin()"><img src="../img/twitter-btn.png" /></a></li>
        </ul>

        </div>
      </div>
    </div>
  </div>
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="../ngApp/lib/jquery-1.9.1.min.js"></script>
    <script src="../ngApp/lib/bootstrap.min.js"></script>    
    <script src="../ngApp/lib/angular.min.js"></script>   
    <script src="../ngApp/lib/angular-sanitize-min.js"></script>   
    
    <script src="../ngApp/lib/angular-route.js"></script>    
    <script>
      angular.module('routers',[])
      .config(["$routeProvider", function ($routeProvider) {
        <?php if(Route::getCurrentRoute()->getPath() == "challenge"){ ?>
        $routeProvider
          .when("/day/1", { templateUrl: '../ngApp/partials/challenge/day-page1.html',controller: "challenge"})
          .when("/day/2", { templateUrl: '../ngApp/partials/challenge/day-page2.html',controller: "challenge"})
          .when("/day/3", { templateUrl: '../ngApp/partials/challenge/day-page3.html',controller: "challenge"})
          .when("/day/4", { templateUrl: '../ngApp/partials/challenge/day-page4.html',controller: "challenge"})
          .when("/day/5", { templateUrl: '../ngApp/partials/challenge/day-page5.html',controller: "challenge"})
          .when("/day/6", { templateUrl: '../ngApp/partials/challenge/day-page6.html',controller: "challenge"})
          .when("/day/7", { templateUrl: '../ngApp/partials/challenge/day-page7.html',controller: "challenge"})
          .otherwise({ redirectTo: '/', templateUrl: '../ngApp/partials/challenge/start-page.html' });
        <?php } ?> 
      }])
    </script>
    <script>
     

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
    </script>    
    <script src="../ngApp/controllers/index.js"></script>
    
  </body>
</html>
