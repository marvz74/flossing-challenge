<?php


/*
|--------------------------------------------------------------------------
| Application Timezone
|--------------------------------------------------------------------------
*/
date_default_timezone_set('Asia/Manila');


/*
|--------------------------------------------------------------------------
| Blade templating
|--------------------------------------------------------------------------
|
| Change the blade templating content tag
|
*/

Blade::setContentTags('{{{', '}}}'); 		// for variables and all things Blade
Blade::setEscapedContentTags('{{{{', '}}}}'); 	// for escaped data


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



//Front-End Routes
Route::group(["before" => "guest"], function(){

	//Home
	Route::get('/', array('uses'			=>	'HomeController@index'));
	Route::get('what', array('uses'			=>	'HomeController@what'));
	Route::get('why', array('uses'			=>	'HomeController@why'));
	Route::get('when', array('uses'			=>	'HomeController@when'));
	Route::get('how', array('uses'			=>	'HomeController@how'));
	Route::get('whatif', array('uses'		=>	'HomeController@whatif'));

	Route::post('setuser', array('uses'		=>	'HomeController@store'));
	Route::get('getuser/{id}', array('uses'		=>	'HomeController@edit'));

	

	//Fields
	Route::get('fields', array('uses' 				=>	'FieldsController@index'));

	Route::get('fields/forms', array('uses' 		=>	'FieldsController@forms'));
	Route::get('fields/create', array('uses' 		=>	'FieldsController@create'));
	Route::post('fields/store', array('uses' 		=>	'FieldsController@store'));
	Route::get('fields/show/{fid}', array('uses' 		=>	'FieldsController@show'));
	Route::get('fields/edit/{fid}', array('uses' 	=>	'FieldsController@edit'));
	Route::get('fields/default/{fid}', array('uses' 	=>	'FieldsController@setDefault'));
	Route::post('fields/update/{fid}', array('uses' 	=>	'FieldsController@update'));
	Route::get('fields/delete/{fid}', array('uses' 	=>	'FieldsController@fieldsDelete'));

	//Challenge
	Route::get('challenge', array('uses'		=>	'ChallengesController@index'));
	Route::post('setuser', array('uses'		=>	'ChallengesController@store'));
	Route::get('getuser/{id}', array('uses'		=>	'ChallengesController@edit'));

});


//API routes
Route::group(array('prefix' => 'api/v1'), function(){

	Route::post('challenge/create', array('uses'	=>	'ChallengeController@create'));
	Route::get('challenge/edit/{id}', array('uses'	=>	'ChallengeController@edit'));
	Route::get('challenge/show', array('uses'	=>	'ChallengeController@show'));
	Route::get('challenge/edit', array('uses'		=>	'ChallengeController@update'));
	Route::get('challenge/delete', array('uses'		=>	'ChallengeController@delete'));
	Route::get('challenge/find', array('uses'		=>	'ChallengeController@find'));

});

